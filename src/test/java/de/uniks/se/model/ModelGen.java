package de.uniks.se.model;

import static org.fulib.builder.ClassModelBuilder.*;

import org.fulib.Fulib;
import org.fulib.builder.ClassBuilder;
import org.fulib.builder.ClassModelBuilder;
import org.fulib.classmodel.ClassModel;
import org.junit.Test;


public class ModelGen {

    private static final String DB_OBJECT_CLASS_NAME = "DBObject";
    private static final String REPOSITORY_CLASS_NAME = "Repository";
    private static final String TASK_CLASS_NAME = "Task";
    private static final String TASKTYPE_CLASS_NAME = "TaskType";
    private static final String CONTAINER_CLASS_NAME = "Container";
    private static final String PARAMETER_CLASS_NAME = "Parameter";
    private static final String WORKLOG_CLASS_NAME = "WorkLog";
    private static final String PERSON_CLASS_NAME = "Person";
    private static final String CREDENTIAL_CLASS_NAME = "Credential";
    private static final String ROLE_CLASS_NAME = "Role";

    private static final String REPOSITORY_ARRAY_LIST = "ArrayList<Repository>";
    private static final String TASK_ARRAY_LIST = "ArrayList<Task>";
    private static final String TASKTYPE_ARRAY_LIST = "ArrayList<TaskType>";
    private static final String CONTAINER_ARRAY_LIST = "ArrayList<Container>";
    private static final String PARAMETER_ARRAY_LIST = "ArrayList<Parameter>";
    private static final String WORKLOG_ARRAY_LIST = "ArrayList<WorkLog>";
    private static final String PERSON_ARRAY_LIST = "ArrayList<Person>";
    private static final String CREDENTIAL_ARRAY_LIST = "ArrayList<Credential>";
    private static final String ROLE_ARRAY_LIST = "ArrayList<Role>";

    private static final String DATE = "Date";
    private static final String STRING_ARRAY_LIST = "ArrayList<String>";

    // ===============================================================================================================

    private ClassModelBuilder mb;
    private ClassBuilder dbObjectmodel;
    private ClassBuilder repoModel;
    private ClassBuilder taskModel;
    private ClassBuilder taskTypeModel;
    private ClassBuilder parameterModel;
    private ClassBuilder containerModel;
    private ClassBuilder workLogModel;
    private ClassBuilder personModel;
    private ClassBuilder credentialModel;
    private ClassBuilder roleModel;

    @Test
    public void testGenerateModel() {
        this.mb = Fulib.classModelBuilder("de.uniks.se.model", "src/main/java");

        this.buildDBObjectModel();
        this.buildRepoModel();
        this.buildTaskModel();
        this.buildTasTypekModel();
        this.buildParameterModel();
        this.buildContainerModel();
        this.buildWorkLogModel();
        this.buildPersonModel();
        this.buildCredentialModel();
        this.buildRoleModel();

        repoModel.setSuperClass(this.dbObjectmodel);
        taskModel.setSuperClass(this.dbObjectmodel);
        taskTypeModel.setSuperClass(this.dbObjectmodel);
        parameterModel.setSuperClass(this.dbObjectmodel);
        containerModel.setSuperClass(this.dbObjectmodel);
        workLogModel.setSuperClass(this.dbObjectmodel);
        personModel.setSuperClass(this.dbObjectmodel);
        credentialModel.setSuperClass(this.dbObjectmodel);
        roleModel.setSuperClass(this.dbObjectmodel);

        // Generate
        ClassModel model = mb.getClassModel();
        Fulib.generator().generate(model);
    }

    private void buildDBObjectModel() {
        this.dbObjectmodel = this.mb.buildClass(DB_OBJECT_CLASS_NAME)
                .buildAttribute("__v", STRING)
                .buildAttribute("_id", STRING)
                .buildAttribute("last_modified_date", DATE)
                .buildAttribute("created_date", DATE);
    }

    private void buildRepoModel() {
        this.repoModel = this.mb.buildClass(REPOSITORY_CLASS_NAME)
                .buildAttribute("name", STRING)
                .buildAttribute("url", STRING)
                // Unidirectional
                .buildAttribute("tasks", TASK_ARRAY_LIST);

    }


    private void buildTaskModel() {
        this.taskModel = this.mb.buildClass(TASK_CLASS_NAME)
                .buildAttribute("title", STRING)
                .buildAttribute("branchName", STRING)
                .buildAttribute("filePath", STRING)
                .buildAttribute("description", STRING)
                .buildAttribute("estimatedPoints", INT)
                // Unidirectional
                .buildAttribute("type", TASKTYPE_CLASS_NAME)
                .buildAttribute("container", CONTAINER_CLASS_NAME)
                .buildAttribute("assignee", PERSON_CLASS_NAME)
                .buildAttribute("parent", TASK_CLASS_NAME)
                .buildAttribute("children", TASK_ARRAY_LIST)
                .buildAttribute("workLogs", WORKLOG_ARRAY_LIST);

    }

    private void buildTasTypekModel() {
        this.taskTypeModel = this.mb.buildClass(TASKTYPE_CLASS_NAME)
                .buildAttribute("name", STRING)
                .buildAttribute("color", STRING)
                .buildAttribute("canLinkFile", BOOLEAN);
    }

    private void buildContainerModel() {
        this.containerModel = this.mb.buildClass(CONTAINER_CLASS_NAME)
                .buildAttribute("name", STRING)
                .buildAttribute("format", STRING)
                .buildAttribute("description", STRING)
                .buildAttribute("url", STRING)
                .buildAttribute("color", STRING)
                // Unidirectional
                .buildAttribute("parameters", PARAMETER_ARRAY_LIST);
    }

    private void buildParameterModel() {
        this.parameterModel = this.mb.buildClass(PARAMETER_CLASS_NAME)
                .buildAttribute("name", STRING)
                .buildAttribute("type", STRING)
                .buildAttribute("example", STRING);
    }


    private void buildWorkLogModel() {
        this.workLogModel = this.mb.buildClass(WORKLOG_CLASS_NAME)
                .buildAttribute("hoursSpend", INT)
                .buildAttribute("minutesSpend", INT)
                .buildAttribute("commitTitle", STRING)
                .buildAttribute("commitDescription", STRING);
    }

    private void buildPersonModel() {
        this.personModel = this.mb.buildClass(PERSON_CLASS_NAME)
                .buildAttribute("firstName", STRING)
                .buildAttribute("lastName", STRING)
                .buildAttribute("username", STRING)
                .buildAttribute("password", STRING)
                .buildAttribute("email", STRING)
                .buildAttribute("personalWords", STRING)
                // Unidirectional
                .buildAttribute("roles", ROLE_ARRAY_LIST)
                .buildAttribute("credentials", CREDENTIAL_ARRAY_LIST);
    }

    private void buildCredentialModel() {
        this.credentialModel = this.mb.buildClass(CREDENTIAL_CLASS_NAME)
                .buildAttribute("username", STRING)
                .buildAttribute("password", STRING)
                // Unidirectional
                .buildAttribute("repository", REPOSITORY_CLASS_NAME);
    }

    private void buildRoleModel() {
        this.roleModel = this.mb.buildClass(ROLE_CLASS_NAME)
                .buildAttribute("name", STRING)
                .buildAttribute("color", STRING);
    }
}
