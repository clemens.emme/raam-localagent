package de.uniks.se;

import de.uniks.se.view.hiddenMenu.HiddenMenuView;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.HashMap;

public class Main extends HiddenMenuView {

    private static final String APPLICATION_TITLE = "Raam-LocalAgent";

    private HashMap<String, String> routeOptions = new HashMap<>();
    public static final String LOGIN_VIEW = "LoginView";
    private static final String LOGIN_VIEW_FXML = "LoginView.fxml";
    public static final String HOME_VIEW = "Home";
    private static final String HOME_VIEW_FXML = "HomeView.fxml";
    public static final String TASKS_VIEW = "Tasks";
    private static final String TASKS_VIEW_FXML = "TasksView.fxml";
    public static final String TASK_VIEW = "Task";
    private static final String TASK_VIEW_FXML = "TaskView.fxml";

    @Override
    public void start(Stage primaryStage) {
        this.primary = primaryStage;
        this.createRoutes();
        this.initViewEnvironment();
        this.routeTo(LOGIN_VIEW);
    }

    private void initViewEnvironment() {
        ViewControlHelper.getInstance().setMain(this);
        this.primary.setTitle(APPLICATION_TITLE);
    }

    private void createRoutes() {
        this.routeOptions = new HashMap<>();
        this.routeOptions.put(LOGIN_VIEW, LOGIN_VIEW_FXML);
        this.routeOptions.put(HOME_VIEW, HOME_VIEW_FXML);
        this.routeOptions.put(TASKS_VIEW, TASKS_VIEW_FXML);
        this.routeOptions.put(TASK_VIEW, TASK_VIEW_FXML);
    }

    void routeTo(String route) {
        try {
            this.loader = new FXMLLoader(getClass().getResource(this.routeOptions.get(route)));
            this.root = loader.load();
            if (this.currentScene != null) {
                this.currentScene = new Scene(this.root, this.currentScene.getWidth(), this.currentScene.getHeight());
            } else {
                this.currentScene = new Scene(this.root);
            }
            this.currentHiddenMenu = loader.getController();
            this.currentScene.setOnKeyPressed(this::listenToKeys);
            this.primary.setScene(this.currentScene);

            if (route.equals(TASK_VIEW)) {
                this.primary.focusedProperty().addListener((observable, oldValue, newValue) -> {
                    if (newValue) {
                        this.callOverlayOfView();
                    }
                });
            }

            this.primary.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    Parent getRoot() {
        return this.root;
    }

    public static void main(String[] args) {
        launch(args);
    }
}
