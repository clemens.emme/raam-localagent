package de.uniks.se.git;

import de.uniks.se.model.Task;
import de.uniks.se.rest.UserClient;
import org.eclipse.jgit.api.CloneCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.ListBranchCommand;
import org.eclipse.jgit.api.MergeResult;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Stream;

public class GitController {

    private static final String HOME_DIRECTORY = System.getProperty("user.home");
    private static final String BACKSLASH = "\\";
    private static final String GIT_DIRECTORY = "gitRepos";

    // ================================================================================================================
    // Singleton
    // ================================================================================================================

    private static GitController controller;

    public static GitController getInstance() {
        if (controller == null) controller = new GitController();
        return controller;
    }

    // ================================================================================================================
    // Fields
    // ================================================================================================================

    private Repository gitRepository;

    // ================================================================================================================
    // Logic
    // ================================================================================================================

    public boolean isRepoAlreadyCloned(String repoName) {
        return new File(buildRepoPath(repoName)).exists();
    }

    public void cloneRepository(de.uniks.se.model.Repository repository) {

        // Handle directory Path
        String pathAsString = buildPathIntoRepo(repository);
        File localPath = this.getInstanciatedDirectory(pathAsString);

        // Get the right credentials
        UsernamePasswordCredentialsProvider userCredentials = UserClient.getUserCredentials(repository);

        // Build up clone Command
        CloneCommand cloneCommand = Git.cloneRepository()
                .setURI(repository.getUrl())
                .setDirectory(localPath)
                .setCloneAllBranches(true)
                .setCredentialsProvider(userCredentials);

        // Clone the Repo
        try (Git result = cloneCommand.call()) {
            this.gitRepository = result.getRepository();

            this.checkBranchStatus(repository);
            System.out.println("Having repository: " + result.getRepository().getDirectory());
        } catch (Exception c) {
            System.err.println(c);
            this.killDirectory(localPath);
        }
    }

    public void openRepository(de.uniks.se.model.Repository repository) {
        // Handle directory Path
        String pathAsString = buildRepoPath(repository);
        File localPath = this.getInstanciatedDirectory(pathAsString);

        FileRepositoryBuilder builder = new FileRepositoryBuilder();
        try {
            this.gitRepository = builder.setGitDir(localPath).readEnvironment().findGitDir().build();
            this.checkBranchStatus(repository);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void checkOutBranch(Task task) {
        if (task.getBranchName() != null && !"".equals(task.getBranchName())) {
            this.checkOutBranch(task.getBranchName());
        }
    }

    private void checkOutBranch(String branchName) {
        try {
            Git git = new Git(this.gitRepository);
            git.checkout().setName(branchName).call();

            this.resolveStashes(git, branchName);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void openFile(de.uniks.se.model.Repository repository, Task task) {
        String fullPath = buildFilePath(buildPathIntoRepo(repository), task.getFilePath());
        File localFile = new File(fullPath);
        try {
            Desktop.getDesktop().edit(localFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public boolean isFileChanges(Task task) {
        try {
            Set<String> modified = new Git(this.gitRepository).status().call().getModified();
            return modified.contains(task.getFilePath());
        } catch (GitAPIException e) {
            e.printStackTrace();
            return false;
        }
    }

    public void commitFile(Task task, String commitTitle) {
        Git git = new Git(this.gitRepository);
        try {
            git.add().addFilepattern(task.getFilePath()).call();
            git.commit().setMessage(commitTitle).call();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void pushCommit(de.uniks.se.model.Repository repository) {
        Git git = new Git(this.gitRepository);
        UsernamePasswordCredentialsProvider userCredentials = UserClient.getUserCredentials(repository);
        try {
            git.push().setCredentialsProvider(userCredentials).call();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // ================================================================================================================
    // Helper
    // ================================================================================================================

    private void checkBranchStatus(de.uniks.se.model.Repository repository) {
        try (Git git = new Git(this.gitRepository)) {

            // fetch remote
            UsernamePasswordCredentialsProvider userCredentials = UserClient.getUserCredentials(repository);
            git.fetch().setCheckFetchedObjects(true).setCredentialsProvider(userCredentials).call();

            ArrayList<Ref> remoteBranches = (ArrayList<Ref>) git.branchList().setListMode(ListBranchCommand.ListMode.REMOTE).call();
            ArrayList<Ref> localBranches = (ArrayList<Ref>) git.branchList().call();


            // make sure the branch is not there
            for (Ref remote : remoteBranches) {
                // if the remote branch is not contained in local branch list, create branch
                if (localBranches.stream().noneMatch((local) -> this.mathingBranchesDoseNotExist(remote, local))) {
                    git.branchCreate().setForce(true).setName(this.getBranchName(remote.getName())).setStartPoint("origin/" + this.getBranchName(remote.getName())).call();
                } else {
                    for (Ref local : localBranches) {
                        if (this.mathingBranchesDoseNotExist(remote, local)) {
                            this.mergeBranches(remote, local, git);
                        }
                    }

                }

            }
        } catch (Exception e) {
            System.err.println(e);
        }
    }

    private void mergeBranches(Ref remote, Ref local, Git git) {
        try {
            // see if there are diffs
            if (git.status().call().getModified().size() > 0) {
                // if so, stash them
                git.stashCreate().call();
            }

            // checkout local
            git.checkout().setName(this.getBranchName(local.getName())).call();

            // Merge remote into local
            git.merge().include(remote).call();
            this.resolveStashes(git, local.getName());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void resolveStashes(Git git, String branchName) throws GitAPIException {
        Collection<RevCommit> stashes = git.stashList().call();
        for (RevCommit stash : stashes) {
            // pop stash of this branch if exist
            if (stash.getFullMessage().contains(this.getBranchName(branchName))) {
                git.stashApply().setStashRef(stash.getName()).call();
                git.stashDrop().setStashRef(0).call();
            }
        }
    }

    private boolean mathingBranchesDoseNotExist(Ref existingBranch, Ref probablyNotExistingBranch) {
        // names are the same
        return this.getBranchName(existingBranch.getName()).equals(this.getBranchName(probablyNotExistingBranch.getName()));
    }

    private String getBranchName(String fullBacnhRef) {
        String branchName;
        String[] splittetRef = fullBacnhRef.split("/");

        branchName = splittetRef[splittetRef.length - 1];

        return branchName;
    }


    private String buildFilePath(String... arguments) {
        String combinedPath = "";

        for (int i = 0; i < arguments.length; i++) {
            if (i != 0) {
                combinedPath += BACKSLASH;
            }
            combinedPath += arguments[i];
        }

        combinedPath = combinedPath.replace("/", BACKSLASH);

        return combinedPath;
    }

    private String buildRepoPath(de.uniks.se.model.Repository repository) {
        return buildRepoPath(repository.getName());
    }

    private String buildRepoPath(String repositoryName) {
        return this.buildPathIntoRepo(repositoryName) + BACKSLASH + ".git";
    }


    private String buildPathIntoRepo(de.uniks.se.model.Repository repository) {
        return buildPathIntoRepo(repository.getName());
    }

    private String buildPathIntoRepo(String repositoryName) {
        return buildFilePath(HOME_DIRECTORY, GIT_DIRECTORY, repositoryName);
    }

    private File getInstanciatedDirectory(String combinedPath) {
        File localPath = new File(combinedPath);

        if (!localPath.exists()) {
            localPath.mkdir();
        }
        return localPath;
    }


    private void killDirectory(String combinedPath) {
        this.killDirectory(new File(combinedPath));
    }

    private void killDirectory(File file) {
        if (file.exists()) file.delete();
    }

}
