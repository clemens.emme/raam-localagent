package de.uniks.se;

import javafx.scene.Parent;

public class ViewControlHelper {
    private static ViewControlHelper single_instance = null;
    // Variables
    public Main mainApplication;

    // ================================================================================================================
    // Singleton
    // ================================================================================================================

    private ViewControlHelper() {
    }

    public static ViewControlHelper getInstance() {
        if (single_instance == null)
            single_instance = new ViewControlHelper();

        return single_instance;
    }

    // ================================================================================================================
    // Logic
    // ================================================================================================================


    public void setMain(Main main) {
        this.mainApplication = main;
    }

    public void routeTo(String route) {
        this.mainApplication.routeTo(route);
    }

    public Parent getCurrentRoot() {
        return this.mainApplication.getRoot();
    }
}
