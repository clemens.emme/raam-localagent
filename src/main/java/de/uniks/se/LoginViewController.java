package de.uniks.se;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import de.uniks.se.model.Person;
import de.uniks.se.rest.UserClient;
import de.uniks.se.view.hiddenMenu.HiddenMenu;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

import java.io.FileReader;
import java.net.URL;
import java.util.HashMap;
import java.util.function.Function;

public class LoginViewController implements HiddenMenu {

    private HashMap<String, Function> secretActions = new HashMap<>();
    private UserClient client = new UserClient();

    @FXML
    TextField username;
    @FXML
    TextField password;

    public LoginViewController() {
        this.secretActions.put("login", (e) -> this.loginDefault());
    }

    // ================================================================================================================
    // Logic
    // ================================================================================================================

    public void pushButton() {
        if (this.username.getText().isEmpty() || this.password.getText().isEmpty()) {
            return;
        }
        client.login(this.username.getText(), this.password.getText(), this::printOutput);
    }

    private Void printOutput(Person person) {
        this.username.setText("");
        this.password.setText("");

        if (person == null) {
            System.err.println("No user with this name");
        } else {
            ViewControlHelper.getInstance().routeTo(Main.HOME_VIEW);
        }

        return null;
    }

    // ================================================================================================================
    // Secret Actions
    // ================================================================================================================

    @Override
    public HashMap<String, Function> getSecretActions() {
        return this.secretActions;
    }

    @Override
    public void showOverlay() {

    }

    private Void loginDefault() {
        try {
            JsonParser parser = new JsonParser();
            URL url = LoginViewController.class.getResource("realUser.json");
            JsonElement element = parser.parse(new FileReader(url.getFile()));
            JsonObject object = element.getAsJsonObject();

            Platform.runLater(() -> {
                this.username.setText(object.get("username").getAsString());
                this.password.setText(object.get("password").getAsString());
                this.pushButton();
            });
        } catch (Exception e) {
            System.err.println(e);
        }
        return null;
    }
}