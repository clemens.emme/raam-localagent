package de.uniks.se.model;

import java.beans.PropertyChangeSupport;

import java.beans.PropertyChangeListener;
import java.util.ArrayList;

public class Repository extends DBObject  
{

   public static final String PROPERTY_name = "name";

   private String name;

   public String getName()
   {
      return name;
   }

   public Repository setName(String value)
   {
      if (value == null ? this.name != null : ! value.equals(this.name))
      {
         String oldValue = this.name;
         this.name = value;
         firePropertyChange("name", oldValue, value);
      }
      return this;
   }


   public static final String PROPERTY_url = "url";

   private String url;

   public String getUrl()
   {
      return url;
   }

   public Repository setUrl(String value)
   {
      if (value == null ? this.url != null : ! value.equals(this.url))
      {
         String oldValue = this.url;
         this.url = value;
         firePropertyChange("url", oldValue, value);
      }
      return this;
   }


   public static final String PROPERTY_tasks = "tasks";

   private ArrayList<Task> tasks;

   public ArrayList<Task> getTasks()
   {
      return tasks;
   }

   public Repository setTasks(ArrayList<Task> value)
   {
      if (value != this.tasks)
      {
         ArrayList<Task> oldValue = this.tasks;
         this.tasks = value;
         firePropertyChange("tasks", oldValue, value);
      }
      return this;
   }


   protected PropertyChangeSupport listeners = null;

   public boolean firePropertyChange(String propertyName, Object oldValue, Object newValue)
   {
      if (listeners != null)
      {
         listeners.firePropertyChange(propertyName, oldValue, newValue);
         return true;
      }
      return false;
   }

   public boolean addPropertyChangeListener(PropertyChangeListener listener)
   {
      if (listeners == null)
      {
         listeners = new PropertyChangeSupport(this);
      }
      listeners.addPropertyChangeListener(listener);
      return true;
   }

   public boolean addPropertyChangeListener(String propertyName, PropertyChangeListener listener)
   {
      if (listeners == null)
      {
         listeners = new PropertyChangeSupport(this);
      }
      listeners.addPropertyChangeListener(propertyName, listener);
      return true;
   }

   public boolean removePropertyChangeListener(PropertyChangeListener listener)
   {
      if (listeners != null)
      {
         listeners.removePropertyChangeListener(listener);
      }
      return true;
   }

   public boolean removePropertyChangeListener(String propertyName,PropertyChangeListener listener)
   {
      if (listeners != null)
      {
         listeners.removePropertyChangeListener(propertyName, listener);
      }
      return true;
   }

   @Override
   public String toString()
   {
      StringBuilder result = new StringBuilder();

      result.append(" ").append(this.getName());
      result.append(" ").append(this.getUrl());


      return result.substring(1);
   }

   public void removeYou()
   {
   }


}