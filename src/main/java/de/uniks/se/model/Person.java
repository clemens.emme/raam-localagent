package de.uniks.se.model;

import java.beans.PropertyChangeSupport;

import java.beans.PropertyChangeListener;
import java.util.ArrayList;

public class Person extends DBObject  
{

   public static final String PROPERTY_firstName = "firstName";

   private String firstName;

   public String getFirstName()
   {
      return firstName;
   }

   public Person setFirstName(String value)
   {
      if (value == null ? this.firstName != null : ! value.equals(this.firstName))
      {
         String oldValue = this.firstName;
         this.firstName = value;
         firePropertyChange("firstName", oldValue, value);
      }
      return this;
   }


   public static final String PROPERTY_lastName = "lastName";

   private String lastName;

   public String getLastName()
   {
      return lastName;
   }

   public Person setLastName(String value)
   {
      if (value == null ? this.lastName != null : ! value.equals(this.lastName))
      {
         String oldValue = this.lastName;
         this.lastName = value;
         firePropertyChange("lastName", oldValue, value);
      }
      return this;
   }


   public static final String PROPERTY_username = "username";

   private String username;

   public String getUsername()
   {
      return username;
   }

   public Person setUsername(String value)
   {
      if (value == null ? this.username != null : ! value.equals(this.username))
      {
         String oldValue = this.username;
         this.username = value;
         firePropertyChange("username", oldValue, value);
      }
      return this;
   }


   public static final String PROPERTY_password = "password";

   private String password;

   public String getPassword()
   {
      return password;
   }

   public Person setPassword(String value)
   {
      if (value == null ? this.password != null : ! value.equals(this.password))
      {
         String oldValue = this.password;
         this.password = value;
         firePropertyChange("password", oldValue, value);
      }
      return this;
   }


   public static final String PROPERTY_email = "email";

   private String email;

   public String getEmail()
   {
      return email;
   }

   public Person setEmail(String value)
   {
      if (value == null ? this.email != null : ! value.equals(this.email))
      {
         String oldValue = this.email;
         this.email = value;
         firePropertyChange("email", oldValue, value);
      }
      return this;
   }


   public static final String PROPERTY_personalWords = "personalWords";

   private String personalWords;

   public String getPersonalWords()
   {
      return personalWords;
   }

   public Person setPersonalWords(String value)
   {
      if (value == null ? this.personalWords != null : ! value.equals(this.personalWords))
      {
         String oldValue = this.personalWords;
         this.personalWords = value;
         firePropertyChange("personalWords", oldValue, value);
      }
      return this;
   }


   public static final String PROPERTY_roles = "roles";

   private ArrayList<Role> roles;

   public ArrayList<Role> getRoles()
   {
      return roles;
   }

   public Person setRoles(ArrayList<Role> value)
   {
      if (value != this.roles)
      {
         ArrayList<Role> oldValue = this.roles;
         this.roles = value;
         firePropertyChange("roles", oldValue, value);
      }
      return this;
   }


   protected PropertyChangeSupport listeners = null;

   public boolean firePropertyChange(String propertyName, Object oldValue, Object newValue)
   {
      if (listeners != null)
      {
         listeners.firePropertyChange(propertyName, oldValue, newValue);
         return true;
      }
      return false;
   }

   public boolean addPropertyChangeListener(PropertyChangeListener listener)
   {
      if (listeners == null)
      {
         listeners = new PropertyChangeSupport(this);
      }
      listeners.addPropertyChangeListener(listener);
      return true;
   }

   public boolean addPropertyChangeListener(String propertyName, PropertyChangeListener listener)
   {
      if (listeners == null)
      {
         listeners = new PropertyChangeSupport(this);
      }
      listeners.addPropertyChangeListener(propertyName, listener);
      return true;
   }

   public boolean removePropertyChangeListener(PropertyChangeListener listener)
   {
      if (listeners != null)
      {
         listeners.removePropertyChangeListener(listener);
      }
      return true;
   }

   public boolean removePropertyChangeListener(String propertyName,PropertyChangeListener listener)
   {
      if (listeners != null)
      {
         listeners.removePropertyChangeListener(propertyName, listener);
      }
      return true;
   }

   @Override
   public String toString()
   {
      StringBuilder result = new StringBuilder();

      result.append(" ").append(this.getFirstName());
      result.append(" ").append(this.getLastName());
      result.append(" ").append(this.getUsername());
      result.append(" ").append(this.getEmail());
      result.append(" ").append(this.getPersonalWords());


      return result.substring(1);
   }

   public void removeYou()
   {
   }


   public static final String PROPERTY_credentials = "credentials";

   private ArrayList<Credential> credentials;

   public ArrayList<Credential> getCredentials()
   {
      return credentials;
   }

   public Person setCredentials(ArrayList<Credential> value)
   {
      if (value != this.credentials)
      {
         ArrayList<Credential> oldValue = this.credentials;
         this.credentials = value;
         firePropertyChange("credentials", oldValue, value);
      }
      return this;
   }


}