package de.uniks.se.model;

import java.beans.PropertyChangeSupport;

import java.beans.PropertyChangeListener;
import java.util.Date;

public class DBObject  
{

   public static final String PROPERTY__id = "_id";

   private String _id;

   public String get_id()
   {
      return _id;
   }

   public DBObject set_id(String value)
   {
      if (value == null ? this._id != null : ! value.equals(this._id))
      {
         String oldValue = this._id;
         this._id = value;
         firePropertyChange("_id", oldValue, value);
      }
      return this;
   }


   public static final String PROPERTY_last_modified_date = "last_modified_date";

   private Date last_modified_date;

   public Date getLast_modified_date()
   {
      return last_modified_date;
   }

   public DBObject setLast_modified_date(Date value)
   {
      if (value != this.last_modified_date)
      {
         Date oldValue = this.last_modified_date;
         this.last_modified_date = value;
         firePropertyChange("last_modified_date", oldValue, value);
      }
      return this;
   }


   public static final String PROPERTY_created_date = "created_date";

   private Date created_date;

   public Date getCreated_date()
   {
      return created_date;
   }

   public DBObject setCreated_date(Date value)
   {
      if (value != this.created_date)
      {
         Date oldValue = this.created_date;
         this.created_date = value;
         firePropertyChange("created_date", oldValue, value);
      }
      return this;
   }


   protected PropertyChangeSupport listeners = null;

   public boolean firePropertyChange(String propertyName, Object oldValue, Object newValue)
   {
      if (listeners != null)
      {
         listeners.firePropertyChange(propertyName, oldValue, newValue);
         return true;
      }
      return false;
   }

   public boolean addPropertyChangeListener(PropertyChangeListener listener)
   {
      if (listeners == null)
      {
         listeners = new PropertyChangeSupport(this);
      }
      listeners.addPropertyChangeListener(listener);
      return true;
   }

   public boolean addPropertyChangeListener(String propertyName, PropertyChangeListener listener)
   {
      if (listeners == null)
      {
         listeners = new PropertyChangeSupport(this);
      }
      listeners.addPropertyChangeListener(propertyName, listener);
      return true;
   }

   public boolean removePropertyChangeListener(PropertyChangeListener listener)
   {
      if (listeners != null)
      {
         listeners.removePropertyChangeListener(listener);
      }
      return true;
   }

   public boolean removePropertyChangeListener(String propertyName,PropertyChangeListener listener)
   {
      if (listeners != null)
      {
         listeners.removePropertyChangeListener(propertyName, listener);
      }
      return true;
   }

   @Override
   public String toString()
   {
      StringBuilder result = new StringBuilder();

      result.append(" ").append(this.get__v());
      result.append(" ").append(this.get_id());


      return result.substring(1);
   }

   public void removeYou()
   {
   }


   public static final String PROPERTY___v = "__v";

   private String __v;

   public String get__v()
   {
      return __v;
   }

   public DBObject set__v(String value)
   {
      if (value == null ? this.__v != null : ! value.equals(this.__v))
      {
         String oldValue = this.__v;
         this.__v = value;
         firePropertyChange("__v", oldValue, value);
      }
      return this;
   }


}