package de.uniks.se.model;

import java.beans.PropertyChangeSupport;

import java.beans.PropertyChangeListener;

public class Credential extends DBObject
{

   public static final String PROPERTY_username = "username";

   private String username;

   public String getUsername()
   {
      return username;
   }

   public Credential setUsername(String value)
   {
      if (value == null ? this.username != null : ! value.equals(this.username))
      {
         String oldValue = this.username;
         this.username = value;
         firePropertyChange("username", oldValue, value);
      }
      return this;
   }


   public static final String PROPERTY_password = "password";

   private String password;

   public String getPassword()
   {
      return password;
   }

   public Credential setPassword(String value)
   {
      if (value == null ? this.password != null : ! value.equals(this.password))
      {
         String oldValue = this.password;
         this.password = value;
         firePropertyChange("password", oldValue, value);
      }
      return this;
   }


   public static final String PROPERTY_repository = "repository";

   private Repository repository;

   public Repository getRepository()
   {
      return repository;
   }

   public Credential setRepository(Repository value)
   {
      if (value != this.repository)
      {
         Repository oldValue = this.repository;
         this.repository = value;
         firePropertyChange("repository", oldValue, value);
      }
      return this;
   }


   protected PropertyChangeSupport listeners = null;

   public boolean firePropertyChange(String propertyName, Object oldValue, Object newValue)
   {
      if (listeners != null)
      {
         listeners.firePropertyChange(propertyName, oldValue, newValue);
         return true;
      }
      return false;
   }

   public boolean addPropertyChangeListener(PropertyChangeListener listener)
   {
      if (listeners == null)
      {
         listeners = new PropertyChangeSupport(this);
      }
      listeners.addPropertyChangeListener(listener);
      return true;
   }

   public boolean addPropertyChangeListener(String propertyName, PropertyChangeListener listener)
   {
      if (listeners == null)
      {
         listeners = new PropertyChangeSupport(this);
      }
      listeners.addPropertyChangeListener(propertyName, listener);
      return true;
   }

   public boolean removePropertyChangeListener(PropertyChangeListener listener)
   {
      if (listeners != null)
      {
         listeners.removePropertyChangeListener(listener);
      }
      return true;
   }

   public boolean removePropertyChangeListener(String propertyName,PropertyChangeListener listener)
   {
      if (listeners != null)
      {
         listeners.removePropertyChangeListener(propertyName, listener);
      }
      return true;
   }

   @Override
   public String toString()
   {
      StringBuilder result = new StringBuilder();

      result.append(" ").append(this.getUsername());
      result.append(" ").append(this.getPassword());


      return result.substring(1);
   }

   public void removeYou()
   {
   }


}