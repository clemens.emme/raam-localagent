package de.uniks.se.model;

import java.beans.PropertyChangeSupport;

import java.beans.PropertyChangeListener;
import java.util.ArrayList;

public class Container extends DBObject {

    public Container() {
    }

    public Container(String id) {
        this.set_id(id);
    }

    public static final String PROPERTY_name = "name";

    private String name;

    public String getName() {
        return name;
    }

    public Container setName(String value) {
        if (value == null ? this.name != null : !value.equals(this.name)) {
            String oldValue = this.name;
            this.name = value;
            firePropertyChange("name", oldValue, value);
        }
        return this;
    }


    public static final String PROPERTY_format = "format";

    private String format;

    public String getFormat() {
        return format;
    }

    public Container setFormat(String value) {
        if (value == null ? this.format != null : !value.equals(this.format)) {
            String oldValue = this.format;
            this.format = value;
            firePropertyChange("format", oldValue, value);
        }
        return this;
    }


    public static final String PROPERTY_description = "description";

    private String description;

    public String getDescription() {
        return description;
    }

    public Container setDescription(String value) {
        if (value == null ? this.description != null : !value.equals(this.description)) {
            String oldValue = this.description;
            this.description = value;
            firePropertyChange("description", oldValue, value);
        }
        return this;
    }


    public static final String PROPERTY_url = "url";

    private String url;

    public String getUrl() {
        return url;
    }

    public Container setUrl(String value) {
        if (value == null ? this.url != null : !value.equals(this.url)) {
            String oldValue = this.url;
            this.url = value;
            firePropertyChange("url", oldValue, value);
        }
        return this;
    }


    protected PropertyChangeSupport listeners = null;

    public boolean firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        if (listeners != null) {
            listeners.firePropertyChange(propertyName, oldValue, newValue);
            return true;
        }
        return false;
    }

    public boolean addPropertyChangeListener(PropertyChangeListener listener) {
        if (listeners == null) {
            listeners = new PropertyChangeSupport(this);
        }
        listeners.addPropertyChangeListener(listener);
        return true;
    }

    public boolean addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        if (listeners == null) {
            listeners = new PropertyChangeSupport(this);
        }
        listeners.addPropertyChangeListener(propertyName, listener);
        return true;
    }

    public boolean removePropertyChangeListener(PropertyChangeListener listener) {
        if (listeners != null) {
            listeners.removePropertyChangeListener(listener);
        }
        return true;
    }

    public boolean removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        if (listeners != null) {
            listeners.removePropertyChangeListener(propertyName, listener);
        }
        return true;
    }

    public void removeYou() {
    }

    public static final String PROPERTY_parameters = "parameters";

    private ArrayList<Parameter> parameters;

    public ArrayList<Parameter> getParameters() {
        return parameters;
    }

    public Container setParameters(ArrayList<Parameter> value) {
        if (value != this.parameters) {
            ArrayList<Parameter> oldValue = this.parameters;
            this.parameters = value;
            firePropertyChange("parameters", oldValue, value);
        }
        return this;
    }


    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();

        result.append(" ").append(this.getName());
        result.append(" ").append(this.getFormat());
        result.append(" ").append(this.getDescription());
        result.append(" ").append(this.getUrl());
        result.append(" ").append(this.getColor());


        return result.substring(1);
    }

    public static final String PROPERTY_color = "color";

    private String color;

    public String getColor() {
        return color;
    }

    public Container setColor(String value) {
        if (value == null ? this.color != null : !value.equals(this.color)) {
            String oldValue = this.color;
            this.color = value;
            firePropertyChange("color", oldValue, value);
        }
        return this;
    }


}