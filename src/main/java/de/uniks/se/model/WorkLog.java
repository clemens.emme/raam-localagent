package de.uniks.se.model;

import java.beans.PropertyChangeSupport;

import java.beans.PropertyChangeListener;

public class WorkLog extends DBObject  
{

   public WorkLog() {}

   public WorkLog(String id) {
      this.set_id(id);
   }

   public static final String PROPERTY_hoursSpend = "hoursSpend";

   private int hoursSpend;

   public int getHoursSpend()
   {
      return hoursSpend;
   }

   public WorkLog setHoursSpend(int value)
   {
      if (value != this.hoursSpend)
      {
         int oldValue = this.hoursSpend;
         this.hoursSpend = value;
         firePropertyChange("hoursSpend", oldValue, value);
      }
      return this;
   }


   public static final String PROPERTY_minutesSpend = "minutesSpend";

   private int minutesSpend;

   public int getMinutesSpend()
   {
      return minutesSpend;
   }

   public WorkLog setMinutesSpend(int value)
   {
      if (value != this.minutesSpend)
      {
         int oldValue = this.minutesSpend;
         this.minutesSpend = value;
         firePropertyChange("minutesSpend", oldValue, value);
      }
      return this;
   }


   public static final String PROPERTY_commitTitle = "commitTitle";

   private String commitTitle;

   public String getCommitTitle()
   {
      return commitTitle;
   }

   public WorkLog setCommitTitle(String value)
   {
      if (value == null ? this.commitTitle != null : ! value.equals(this.commitTitle))
      {
         String oldValue = this.commitTitle;
         this.commitTitle = value;
         firePropertyChange("commitTitle", oldValue, value);
      }
      return this;
   }


   public static final String PROPERTY_commitDescription = "commitDescription";

   private String commitDescription;

   public String getCommitDescription()
   {
      return commitDescription;
   }

   public WorkLog setCommitDescription(String value)
   {
      if (value == null ? this.commitDescription != null : ! value.equals(this.commitDescription))
      {
         String oldValue = this.commitDescription;
         this.commitDescription = value;
         firePropertyChange("commitDescription", oldValue, value);
      }
      return this;
   }


   protected PropertyChangeSupport listeners = null;

   public boolean firePropertyChange(String propertyName, Object oldValue, Object newValue)
   {
      if (listeners != null)
      {
         listeners.firePropertyChange(propertyName, oldValue, newValue);
         return true;
      }
      return false;
   }

   public boolean addPropertyChangeListener(PropertyChangeListener listener)
   {
      if (listeners == null)
      {
         listeners = new PropertyChangeSupport(this);
      }
      listeners.addPropertyChangeListener(listener);
      return true;
   }

   public boolean addPropertyChangeListener(String propertyName, PropertyChangeListener listener)
   {
      if (listeners == null)
      {
         listeners = new PropertyChangeSupport(this);
      }
      listeners.addPropertyChangeListener(propertyName, listener);
      return true;
   }

   public boolean removePropertyChangeListener(PropertyChangeListener listener)
   {
      if (listeners != null)
      {
         listeners.removePropertyChangeListener(listener);
      }
      return true;
   }

   public boolean removePropertyChangeListener(String propertyName,PropertyChangeListener listener)
   {
      if (listeners != null)
      {
         listeners.removePropertyChangeListener(propertyName, listener);
      }
      return true;
   }

   @Override
   public String toString()
   {
      StringBuilder result = new StringBuilder();

      result.append(" ").append(this.getCommitTitle());
      result.append(" ").append(this.getCommitDescription());


      return result.substring(1);
   }

   public void removeYou()
   {
   }


}