package de.uniks.se.model;

import java.beans.PropertyChangeSupport;

import java.beans.PropertyChangeListener;
import java.util.ArrayList;

public class Task extends DBObject {

    public Task() {}

    public Task(String id) {
        this.set_id(id);
    }

    public static final String PROPERTY_title = "title";

    private String title;

    public String getTitle() {
        return title;
    }

    public Task setTitle(String value) {
        if (value == null ? this.title != null : !value.equals(this.title)) {
            String oldValue = this.title;
            this.title = value;
            firePropertyChange("title", oldValue, value);
        }
        return this;
    }


    public static final String PROPERTY_branchName = "branchName";

    private String branchName;

    public String getBranchName() {
        return branchName;
    }

    public Task setBranchName(String value) {
        if (value == null ? this.branchName != null : !value.equals(this.branchName)) {
            String oldValue = this.branchName;
            this.branchName = value;
            firePropertyChange("branchName", oldValue, value);
        }
        return this;
    }


    public static final String PROPERTY_filePath = "filePath";

    private String filePath;

    public String getFilePath() {
        return filePath;
    }

    public Task setFilePath(String value) {
        if (value == null ? this.filePath != null : !value.equals(this.filePath)) {
            String oldValue = this.filePath;
            this.filePath = value;
            firePropertyChange("filePath", oldValue, value);
        }
        return this;
    }


    public static final String PROPERTY_description = "description";

    private String description;

    public String getDescription() {
        return description;
    }

    public Task setDescription(String value) {
        if (value == null ? this.description != null : !value.equals(this.description)) {
            String oldValue = this.description;
            this.description = value;
            firePropertyChange("description", oldValue, value);
        }
        return this;
    }


    public static final String PROPERTY_estimatedPoints = "estimatedPoints";

    private int estimatedPoints;

    public int getEstimatedPoints() {
        return estimatedPoints;
    }

    public Task setEstimatedPoints(int value) {
        if (value != this.estimatedPoints) {
            int oldValue = this.estimatedPoints;
            this.estimatedPoints = value;
            firePropertyChange("estimatedPoints", oldValue, value);
        }
        return this;
    }


    public static final String PROPERTY_type = "type";

    private TaskType type;

    public TaskType getType() {
        return type;
    }

    public Task setType(TaskType value) {
        if (value != this.type) {
            TaskType oldValue = this.type;
            this.type = value;
            firePropertyChange("type", oldValue, value);
        }
        return this;
    }


    public static final String PROPERTY_container = "container";

    private Container container;

    public Container getContainer() {
        return container;
    }

    public Task setContainer(Container value) {
        if (value != this.container) {
            Container oldValue = this.container;
            this.container = value;
            firePropertyChange("container", oldValue, value);
        }
        return this;
    }


    public static final String PROPERTY_assignee = "assignee";

    private Person assignee;

    public Person getAssignee() {
        return assignee;
    }

    public Task setAssignee(Person value) {
        if (value != this.assignee) {
            Person oldValue = this.assignee;
            this.assignee = value;
            firePropertyChange("assignee", oldValue, value);
        }
        return this;
    }


    public static final String PROPERTY_parent = "parent";

    private Task parent;

    public Task getParent() {
        return parent;
    }

    public Task setParent(Task value) {
        if (value != this.parent) {
            Task oldValue = this.parent;
            this.parent = value;
            firePropertyChange("parent", oldValue, value);
        }
        return this;
    }


    public static final String PROPERTY_children = "children";

    private ArrayList<Task> children;

    public ArrayList<Task> getChildren() {
        return children;
    }

    public Task setChildren(ArrayList<Task> value) {
        if (value != this.children) {
            ArrayList<Task> oldValue = this.children;
            this.children = value;
            firePropertyChange("children", oldValue, value);
        }
        return this;
    }


    public static final String PROPERTY_workLogs = "workLogs";

    private ArrayList<WorkLog> workLogs;

    public ArrayList<WorkLog> getWorkLogs() {
        return workLogs;
    }

    public Task setWorkLogs(ArrayList<WorkLog> value) {
        if (value != this.workLogs) {
            ArrayList<WorkLog> oldValue = this.workLogs;
            this.workLogs = value;
            firePropertyChange("workLogs", oldValue, value);
        }
        return this;
    }


    protected PropertyChangeSupport listeners = null;

    public boolean firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        if (listeners != null) {
            listeners.firePropertyChange(propertyName, oldValue, newValue);
            return true;
        }
        return false;
    }

    public boolean addPropertyChangeListener(PropertyChangeListener listener) {
        if (listeners == null) {
            listeners = new PropertyChangeSupport(this);
        }
        listeners.addPropertyChangeListener(listener);
        return true;
    }

    public boolean addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        if (listeners == null) {
            listeners = new PropertyChangeSupport(this);
        }
        listeners.addPropertyChangeListener(propertyName, listener);
        return true;
    }

    public boolean removePropertyChangeListener(PropertyChangeListener listener) {
        if (listeners != null) {
            listeners.removePropertyChangeListener(listener);
        }
        return true;
    }

    public boolean removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        if (listeners != null) {
            listeners.removePropertyChangeListener(propertyName, listener);
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();

        result.append(" ").append(this.getTitle());
        result.append(" ").append(this.getBranchName());
        result.append(" ").append(this.getFilePath());
        result.append(" ").append(this.getDescription());


        return result.substring(1);
    }

    public void removeYou() {
    }


}