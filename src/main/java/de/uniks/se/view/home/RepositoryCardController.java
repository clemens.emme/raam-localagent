package de.uniks.se.view.home;

import de.uniks.se.Main;
import de.uniks.se.ViewControlHelper;
import de.uniks.se.git.GitController;
import de.uniks.se.model.Repository;
import de.uniks.se.rest.RepositoryClient;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;


public class RepositoryCardController {

    @FXML
    public Label repoName;

    @FXML
    public Button cloneButton;

    @FXML
    public Button selectButton;

    private Repository repository;
    private GitController git;

    public RepositoryCardController() {
        this.git = GitController.getInstance();
    }

    @FXML
    void initialize() {
    }

    // ================================================================================================================
    // Logic
    // ================================================================================================================

    public void selectRepo() {
        RepositoryClient.getOneByName(this.repository.getName());
        ViewControlHelper.getInstance().routeTo(Main.TASKS_VIEW);
    }

    public void coleRepo() {
        git.cloneRepository(this.repository);
        this.checkForRepository();
    }

    private void checkForRepository() {
        boolean repoIsCloned = git.isRepoAlreadyCloned(this.repository.getName());
        this.buttonFlipFlop(repoIsCloned);
    }

    private void buttonFlipFlop(boolean repoIsCloned) {
        Platform.runLater(() -> {
            this.cloneButton.setVisible(!repoIsCloned);
            this.selectButton.setVisible(repoIsCloned);
        });
    }

    // ================================================================================================================
    // Generated
    // ================================================================================================================

    void setRepository(Repository repository) {
        this.repository = repository;
        this.repoName.setText(this.repository.getName());
        this.checkForRepository();
    }
}
