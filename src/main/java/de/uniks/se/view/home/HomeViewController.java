package de.uniks.se.view.home;

import de.uniks.se.Main;
import de.uniks.se.ViewControlHelper;
import de.uniks.se.model.Repository;
import de.uniks.se.rest.RepositoryClient;
import de.uniks.se.view.hiddenMenu.HiddenMenu;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.function.Function;

public class HomeViewController implements HiddenMenu {

    private static final String REPO_CARD_FXML = "RepositoryCard.fxml";
    
    private HashMap<String, Function> secretActions = new HashMap<>();
    private ArrayList<Repository> repos = new ArrayList<>();

    @FXML
    private VBox repoCardList;


    // ================================================================================================================
    // Init
    // ================================================================================================================

    public HomeViewController() {
        this.secretActions.put("test", (e) -> this.test());
        this.secretActions.put("logout", (e) -> this.logout());
    }

    @FXML
    void initialize() {
        this.createRepoCards();
        for (Repository repo : this.repos) {
            HBox hBox = new HBox();
            hBox.setPrefHeight(200);
            hBox.getChildren().add(this.loadRepoCard(repo));
            this.repoCardList.getChildren().add(hBox);
        }
    }

    private Parent loadRepoCard(Repository repo) {
        try {
            FXMLLoader loader = getNewRepoCardLoader();
            Parent root = loader.load();
            RepositoryCardController controller = loader.getController();
            controller.setRepository(repo);
            return root;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    // ================================================================================================================
    // Logic
    // ================================================================================================================

    private void createRepoCards() {
        RepositoryClient.getAll(this::setRepoList);
    }

    private Void setRepoList(ArrayList<Repository> repos) {
        this.repos = repos;
        return null;
    }

    private FXMLLoader getNewRepoCardLoader() {
        return new FXMLLoader(getClass().getResource(REPO_CARD_FXML));
    }

    // ================================================================================================================
    // Routing
    // ================================================================================================================

    private Void logout() {
        ViewControlHelper.getInstance().routeTo(Main.LOGIN_VIEW);
        return null;
    }

    // ================================================================================================================
    // Secret Actions
    // ================================================================================================================

    @Override
    public HashMap<String, Function> getSecretActions() {
        return this.secretActions;
    }

    @Override
    public void showOverlay() {

    }


    private Void test() {
        System.out.println("Test");
        return null;
    }

}