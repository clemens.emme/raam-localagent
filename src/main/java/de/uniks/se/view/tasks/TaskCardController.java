package de.uniks.se.view.tasks;

import de.uniks.se.Main;
import de.uniks.se.ViewControlHelper;
import de.uniks.se.model.Task;
import de.uniks.se.rest.RepositoryClient;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class TaskCardController {

    @FXML
    public VBox textBox;
    @FXML
    public Label taskLabel;
    @FXML
    public Label taskTypLabel;

    // ================================================================================================================

    @FXML
    public HBox titleBox;
    @FXML
    public HBox userBox;
    @FXML
    public Label userLabel;

    // ================================================================================================================

    @FXML
    public VBox issueTag;
    @FXML
    public VBox formatBox;
    @FXML
    public Label assetTypeLabel;

    // ================================================================================================================
    @FXML
    public HBox descBox;
    @FXML
    public Label descLabel;

    private Task task;
    // ================================================================================================================
    // Init
    // ================================================================================================================

    public void switchDescription(MouseEvent e) {
        if (textBox.getChildren().size() > 1) {
            this.resetAssignee();
            this.resetFormat();
            this.resetDescription();
        } else {
            this.setAssignee();
            this.setFormat();
            this.setDescription();

        }
    }

    void setTask(Task task) {
        this.task = task;
        this.taskLabel.setText(this.task.getTitle());
        this.setTaskType();
        this.resetAssignee();
        this.resetFormat();
        this.resetDescription();
    }

    private void setTaskType() {
        this.taskTypLabel.setText(this.task.getType().getName());
        this.issueTag.setStyle("-fx-background-color: " + this.task.getType().getColor() + ";");
    }

    // ================================================================================================================
    // Logic
    // ================================================================================================================

    private void setAssignee() {
        if (this.task.getAssignee() != null) {
            this.userLabel.setText(this.task.getAssignee().getUsername());
            this.userBox.getChildren().add(1, this.userLabel);
        }
    }

    private void resetAssignee() {
        this.userBox.getChildren().remove(this.userLabel);
    }

    private void setFormat() {
        if (this.task.getContainer() != null) {
            this.assetTypeLabel.setText(this.task.getContainer().getFormat());
            this.issueTag.getChildren().add(this.formatBox);
        }
    }

    private void resetFormat() {
        this.issueTag.getChildren().remove(this.formatBox);
    }

    private void setDescription() {
        this.descLabel.setText(this.task.getDescription());
        this.textBox.getChildren().add(this.descBox);
    }

    private void resetDescription() {
        this.textBox.getChildren().remove(this.descBox);
    }

    // ================================================================================================================
    // Route
    // ================================================================================================================

    public void routeToTask() {
        RepositoryClient.setActiveTask(this.task);
        ViewControlHelper.getInstance().routeTo(Main.TASK_VIEW);
    }
}
