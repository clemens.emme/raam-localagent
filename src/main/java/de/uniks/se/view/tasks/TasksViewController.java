package de.uniks.se.view.tasks;

import de.uniks.se.Main;
import de.uniks.se.ViewControlHelper;
import de.uniks.se.model.Repository;
import de.uniks.se.model.Task;
import de.uniks.se.rest.RepositoryClient;
import de.uniks.se.view.hiddenMenu.HiddenMenu;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

import java.io.IOException;
import java.util.HashMap;
import java.util.function.Function;

public class TasksViewController implements HiddenMenu {

    private static final String TASK_CARD_FXML = "TaskCard.fxml";

    private HashMap<String, Function> secretActions = new HashMap<>();
    private Repository repository;

    @FXML
    private Label selectedRepoName;
    @FXML
    private VBox taskCardList;


    // ================================================================================================================
    // Init
    // ================================================================================================================

    public TasksViewController() {
        this.secretActions.put("test", (e) -> this.test());
        this.secretActions.put("home", (e) -> this.routeHome());
        this.secretActions.put("logout", (e) -> this.logout());
    }

    @FXML
    void initialize() {
        this.getActiveRepo();
        for (Task task : this.repository.getTasks()) {
            taskCardList.getChildren().add(this.loadTaskCard(task));
        }
    }

    private Parent loadTaskCard(Task task) {
        try {
            FXMLLoader loader = getNewTaskCardLoader();
            Parent root = loader.load();
            TaskCardController controller = loader.getController();
            controller.setTask(task);
            return root;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    // ================================================================================================================
    // Logic
    // ================================================================================================================

    public void getActiveRepo() {
        this.repository = RepositoryClient.getActiveRepo();
        this.selectedRepoName.setText(this.repository.getName());
    }

    private FXMLLoader getNewTaskCardLoader() {
        return new FXMLLoader(getClass().getResource(TASK_CARD_FXML));
    }

    // ================================================================================================================
    // Routing
    // ================================================================================================================

    private Void routeHome() {
        ViewControlHelper.getInstance().routeTo(Main.HOME_VIEW);
        return null;
    }

    private Void logout() {
        ViewControlHelper.getInstance().routeTo(Main.LOGIN_VIEW);
        return null;
    }

    // ================================================================================================================
    // Secret Actions
    // ================================================================================================================

    @Override
    public HashMap<String, Function> getSecretActions() {
        return this.secretActions;
    }

    @Override
    public void showOverlay() {

    }


    private Void test() {
        System.out.println("Test");
        return null;
    }
}
