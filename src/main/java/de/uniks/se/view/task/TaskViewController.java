package de.uniks.se.view.task;

import de.uniks.se.Main;
import de.uniks.se.ViewControlHelper;
import de.uniks.se.git.GitController;
import de.uniks.se.model.Repository;
import de.uniks.se.model.Task;
import de.uniks.se.model.WorkLog;
import de.uniks.se.rest.RepositoryClient;
import de.uniks.se.rest.TaskClient;
import de.uniks.se.rest.WorkLogClient;
import de.uniks.se.view.hiddenMenu.HiddenMenu;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.function.Function;

public class TaskViewController implements HiddenMenu {

    private final GitController git;
    private HashMap<String, Function> secretActions = new HashMap<>();

    private Repository repository;
    private Task task;

    // ================================================================================================================
    @FXML
    private Label selectedRepoName;

    @FXML
    private VBox taskBox;

    // ================================================================================================================
    @FXML
    private VBox issueBox;
    @FXML
    private Label taskTypeLabel;

    @FXML
    private VBox formatBox;
    @FXML
    private Label assetTypeLabel;

    // ================================================================================================================
    @FXML
    private Label titleLabel;
    @FXML
    private Label descriptionLabel;

    // ================================================================================================================
    @FXML
    private HBox metadataBox;

    @FXML
    private Label estimatedPointsLabel;
    @FXML
    private Label estimatedTimeLabel;
    @FXML
    private Label spendTimeLabel;

    @FXML
    private VBox userBox;
    @FXML
    private Label fullNameLabel;
    @FXML
    private Label usernameLabel;

    // ================================================================================================================

    @FXML
    private HBox fileBoxParent;

    @FXML
    private VBox fileBox;

    @FXML
    private HBox fileBorder;

    @FXML
    private Label filePathLabel;

    // ================================================================================================================
    @FXML
    private HBox commitOverlay;

    @FXML
    private TextField hoursSpendTextField;

    @FXML
    private TextField minutesSpendTextField;

    @FXML
    private TextField commitTitleTextField;

    @FXML
    private TextArea commitDescriptionTextArea;

    // ================================================================================================================
    // Init
    // ================================================================================================================

    public TaskViewController() {
        this.git = GitController.getInstance();
        this.secretActions.put("test", (e) -> this.test());
        this.secretActions.put("task list", (e) -> this.routeToTaskList());
        this.secretActions.put("home", (e) -> this.routeHome());
        this.secretActions.put("logout", (e) -> this.logout());
    }

    @FXML
    void initialize() {
        this.task = RepositoryClient.getActiveTask();
        this.repository = RepositoryClient.getActiveRepo();
        this.initRepoViewElements();
        this.initTaskViewElements();
        this.git.openRepository(this.repository);
        this.git.checkOutBranch(this.task);
        this.onFocusChange();
    }

    private void initRepoViewElements() {
        this.selectedRepoName.setText(this.repository.getName());
    }

    private void initTaskViewElements() {
        this.titleLabel.setText(this.task.getTitle());
        this.descriptionLabel.setText(this.task.getDescription());
        this.setTaskTypeContent();
        this.setMetadataContent();
        this.setFileContent();
    }

    private void setTaskTypeContent() {
        this.taskTypeLabel.setText(this.task.getType().getName());
        this.issueBox.setStyle("-fx-background-color: " + this.task.getType().getColor() + ";");
        if (this.task.getContainer() != null) {
            this.assetTypeLabel.setText(this.task.getContainer().getFormat());
        } else {
            this.issueBox.getChildren().remove(this.formatBox);
        }
    }


    private void setMetadataContent() {
        this.estimatedPointsLabel.setText("" + this.task.getEstimatedPoints());
        this.estimatedTimeLabel.setText(this.getEstimatedTime());
        this.spendTimeLabel.setText(this.getSpendTime());
        if (this.task.getAssignee() != null) {
            this.fullNameLabel.setText(this.getFullName());
            this.usernameLabel.setText(this.task.getAssignee().getUsername());
        } else {
            this.metadataBox.getChildren().remove(this.userBox);
        }
    }

    private String getFullName() {
        return this.task.getAssignee().getFirstName() + " " + this.task.getAssignee().getLastName();
    }

    private String getSpendTime() {
        return "0h 0m";
    }

    private String getEstimatedTime() {
        return "0h 0m";
    }


    private void setFileContent() {
        if (this.task.getFilePath().isEmpty()) {
            this.taskBox.getChildren().remove(this.fileBoxParent);
        } else {
            this.filePathLabel.setText(this.task.getFilePath());
            if (this.task.getContainer() != null) {
                this.fileBorder.setStyle("-fx-border-color: " + this.task.getType().getColor() + ";");
            } else {
                this.fileBorder.setStyle("-fx-border-color: #fff;");
            }

            this.fileBox.setOnMouseClicked(event -> this.openFile());
        }
    }

    // ================================================================================================================
    // Logic
    // ================================================================================================================


    private void openFile() {
        git.openFile(this.repository, this.task);
    }

    private void onFocusChange() {
        if (git.isFileChanges(this.task)) {
            this.openCommitOverlay();
        }
    }

    private void openCommitOverlay() {
        this.commitOverlay.setVisible(true);
        this.commitOverlay.setMouseTransparent(false);
    }

    public void commitChanges() {
        // Check for proper input
        boolean titleWrong = this.commitTitleTextField.getText().isEmpty();
        boolean hoursWrong = this.isNotNumber(this.hoursSpendTextField);
        boolean minutesWrong = this.isNotNumber(this.minutesSpendTextField);

        this.ifWrongThanDisplay(titleWrong, this.commitTitleTextField);
        this.ifWrongThanDisplay(hoursWrong, this.hoursSpendTextField);
        this.ifWrongThanDisplay(minutesWrong, this.minutesSpendTextField);

        if (titleWrong || hoursWrong || minutesWrong) return;

        //create a Work Log
        WorkLog log = new WorkLog();
        log.setCommitTitle(this.commitTitleTextField.getText());
        log.setCommitDescription(this.commitDescriptionTextArea.getText());
        log.setHoursSpend(this.convertToNumber(this.hoursSpendTextField));
        log.setMinutesSpend(this.convertToNumber(this.minutesSpendTextField));

        // push work Log
        WorkLogClient.postWorkLog(log, this::uploadTaskPhase);

    }

    private Void uploadTaskPhase(WorkLog logInDb) {

        // Update Task
        ArrayList<WorkLog> workLogs = this.task.getWorkLogs();
        workLogs.add(logInDb);
        this.task.setWorkLogs(workLogs);
        TaskClient.putTask(this.task, this::commitPhase);

        return null;
    }

    private Void commitPhase(Task task) {
        this.task = task;

        // commit the file
        this.git.commitFile(this.task, this.commitTitleTextField.getText());

        // push commit
        this.git.pushCommit(this.repository);


        // rest input
        this.commitTitleTextField.setText("");
        this.commitDescriptionTextArea.setText("");
        this.hoursSpendTextField.setText("");
        this.minutesSpendTextField.setText("");
        this.closeCommitOverlay();
        return null;
    }

    public void closeCommitOverlay() {
        this.commitOverlay.setVisible(false);
        this.commitOverlay.setMouseTransparent(true);
    }

    // ================================================================================================================
    // Routing
    // ================================================================================================================

    private Void routeToTaskList() {
        ViewControlHelper.getInstance().routeTo(Main.TASKS_VIEW);
        return null;
    }

    private Void routeHome() {
        ViewControlHelper.getInstance().routeTo(Main.HOME_VIEW);
        return null;
    }

    private Void logout() {
        ViewControlHelper.getInstance().routeTo(Main.LOGIN_VIEW);
        return null;
    }

    // ================================================================================================================
    // Helper
    // ================================================================================================================

    private boolean isNotNumber(TextField textfield) {
        if (textfield.getText().isEmpty()) {
            textfield.setText("0");
        }
        return !textfield.getText().matches("[0-9]+");
    }


    private int convertToNumber(TextField textfield) {
        int number = 0;

        try {
            return Integer.parseInt(textfield.getText());
        } catch (Exception e) {
            e.printStackTrace();
            return number;
        }

    }

    private void setWrongInformationBorder(TextField textfield) {
        textfield.getStyleClass().add("wrongInfo");
    }

    private void removeWrongInformationBorder(TextField textfield) {
        textfield.getStyleClass().remove("wrongInfo");
    }

    private void ifWrongThanDisplay(boolean isWrong, TextField textField) {
        if (isWrong) {
            this.setWrongInformationBorder(textField);
        } else {
            this.removeWrongInformationBorder(textField);
        }
    }

    // ================================================================================================================
    // Secret Actions
    // ================================================================================================================

    @Override
    public HashMap<String, Function> getSecretActions() {
        return this.secretActions;
    }


    @Override
    public void showOverlay() {
        this.onFocusChange();
    }


    private Void test() {
        System.out.println("Test");
        return null;
    }
}
