package de.uniks.se.view.hiddenMenu;


import java.util.HashMap;
import java.util.function.Function;

public interface HiddenMenu {

    public HashMap<String, Function> secretActions = new HashMap<>();

    abstract HashMap<String, Function> getSecretActions();

    abstract void showOverlay();
}
