package de.uniks.se.view.hiddenMenu;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

import java.util.HashMap;
import java.util.function.Function;

public abstract class HiddenMenuView extends Application {

    protected FXMLLoader loader;
    protected Parent root;
    protected Stage primary;
    protected Scene currentScene;

    protected HiddenMenu currentHiddenMenu;
    protected Node lookup;
    protected TextField hiddenMenuText;
    protected Label secretMenuCommandList;

    public HashMap<String, Function> globalSecretActions = new HashMap<>();

    public HiddenMenuView() {
        this.globalSecretActions.put("list", (e) -> this.listAllCommands());
        this.globalSecretActions.put("close", (e) -> this.changeVisibility());
    }

    protected void listenToKeys(KeyEvent event) {
        if (event.getCode() == KeyCode.ESCAPE) {
            this.lookup = this.currentScene.lookup("#secretMenu");
            this.secretMenuCommandList = (Label) this.currentScene.lookup("#secretMenuCommandList");
            this.hiddenMenuText = (TextField) this.currentScene.lookup("#secretMenuText");
            this.changeVisibility();
        }

        if (event.getCode() == KeyCode.ENTER && lookup.isVisible()) {
            Function function = currentHiddenMenu.getSecretActions().get(this.hiddenMenuText.getText().toLowerCase());
            if (function != null) {
                function.apply(null);
                this.hiddenMenuText.setText("");
            }
            function = globalSecretActions.get(this.hiddenMenuText.getText().toLowerCase());
            if (function != null) {
                function.apply(null);
                this.hiddenMenuText.setText("");
            }
        }
    }

    protected Void listAllCommands() {
        if ("".equals(this.secretMenuCommandList.getText())) {
            String commands = "";
            for (HashMap.Entry<String, Function> command : globalSecretActions.entrySet()) {
                commands += command.getKey() + ", ";
            }
            for (HashMap.Entry<String, Function> command : currentHiddenMenu.getSecretActions().entrySet()) {
                commands += command.getKey() + ", ";
            }
            this.secretMenuCommandList.setText(commands);
        } else {
            this.secretMenuCommandList.setText("");
        }
        return null;
    }

    protected Void changeVisibility() {
        lookup.setVisible(!lookup.isVisible());
        return null;
    }

    protected void callOverlayOfView() {
        this.currentHiddenMenu.showOverlay();
    }
}
