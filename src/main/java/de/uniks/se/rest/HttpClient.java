package de.uniks.se.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import de.uniks.se.model.DBObject;
import de.uniks.se.model.Person;
import okhttp3.*;

import java.io.IOException;
import java.util.List;

public class HttpClient {

    // ================================================================================================================
    // Singleton
    // ================================================================================================================

    private static OkHttpClient client;
    private static Gson gson;
    private static ObjectMapper mapper;

    private HttpClient() {
        client = new OkHttpClient();
    }

    public static OkHttpClient getInstance() {
        if (client == null) client = new OkHttpClient();
        if (gson == null) gson = new Gson();

        return client;
    }

    public static Gson serialize() {
        if (gson == null) gson = new Gson();

        return gson;
    }

    public static ObjectMapper deserialize() {
        if (mapper == null) mapper = new ObjectMapper();

        return mapper;
    }

    // ================================================================================================================
    // parsins
    // ================================================================================================================

    public static List deserializeList(String jsonInput, Class myClass) {
        List list = null;
        try {
            list = deserialize().readValue(jsonInput, deserialize().getTypeFactory().constructCollectionType(List.class, myClass));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }

    // ================================================================================================================
    // Rest
    // ================================================================================================================

    public static String get(String url) {

        // Request
        Request request = new Request.Builder()
                .url(url)
                .get()
                .addHeader("cache-control", "no-cache")
                .build();

        // Response
        try (Response response = getInstance().newCall(request).execute()) {
            return response.body().string();
        } catch (Exception e) {
            System.err.println("get " + url + " " + e.toString());
            return "";
        }
    }


    public static String post(String url, DBObject object) {
        return put(url, serialize().toJson(object));
    }

    public static String post(String url, String json) {

        // Content
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, json);

        // Request
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .addHeader("content-type", "application/json")
                .addHeader("cache-control", "no-cache")
                .build();

        // Response
        try (Response response = getInstance().newCall(request).execute()) {
            return response.body().string();
        } catch (Exception e) {
            System.err.println(e.toString());
            return "";
        }
    }

    public static String put(String url, DBObject object) {
        return put(url, serialize().toJson(object));
    }

    public static String put(String url, String json) {

        // Content
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, json);

        // Request
        Request request = new Request.Builder()
                .url(url)
                .put(body)
                .addHeader("content-type", "application/json")
                .addHeader("cache-control", "no-cache")
                .build();

        // Response
        try (Response response = getInstance().newCall(request).execute()) {
            return response.body().string();
        } catch (Exception e) {
            System.err.println(e.toString());
            return "";
        }
    }

    public static String delete(String url) {

        // Request
        Request request = new Request.Builder()
                .url(url)
                .delete()
                .addHeader("cache-control", "no-cache")
                .build();

        // Response
        try (Response response = getInstance().newCall(request).execute()) {
            return response.body().string();
        } catch (Exception e) {
            System.err.println(e.toString());
            return "";
        }
    }
}