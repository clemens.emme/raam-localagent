package de.uniks.se.rest;

import static de.uniks.se.rest.RestUtils.*;

import de.uniks.se.model.Person;
import de.uniks.se.model.Repository;
import de.uniks.se.model.Task;

import java.io.IOException;
import java.util.ArrayList;
import java.util.function.Function;

public class RepositoryClient {

    private static ArrayList<Repository> repositorys;
    private static Repository repository;
    private static Task task;


    // ================================================================================================================
    // Rest
    // ================================================================================================================

    public static void getAll(Function<ArrayList<Repository>, Void> callback) {
        String url = DB_BASE_URL + DB_REPOSITORY;

        String repos = HttpClient.get(url);

        repositorys = (ArrayList<Repository>) HttpClient.deserializeList(repos, Repository.class);

        callback.apply(repositorys);
    }

    public static void getOneById(String id) {
        getOneById(id, RepositoryClient::defaultCallback);
    }

    public static void getOneById(String id, Function<Repository, Void> callback) {
        String url = DB_BASE_URL + DB_REPOSITORY + "/" + id;

        getSingleRepo(callback, url);
    }


    public static void getOneByName(String name) {
        getOneByName(name, RepositoryClient::defaultCallback);
    }

    public static void getOneByName(String name, Function<Repository, Void> callback) {
        String url = DB_BASE_URL + DB_REPOSITORY + "/" + name;

        getSingleRepo(callback, url);

    }

    private static void getSingleRepo(Function<Repository, Void> callback, String url) {
        try {
            String repos = HttpClient.get(url);
            repository = HttpClient.deserialize().readValue(repos, Repository.class);
            callback.apply(repository);
        } catch (IOException e) {
            callback.apply(null);
            e.printStackTrace();
        }
    }

    public static Void defaultCallback(Repository repo) {
        return null;
    }

    // ================================================================================================================
    // Logic
    // ================================================================================================================

    public static void reset() {
        task = null;
        repository = null;
    }

    public static Repository getActiveRepo() {
        return repository;
    }

    public static void setActiveTask(Task activeTask) {
        task = activeTask;
    }

    public static Task getActiveTask() {
        return task;
    }
}
