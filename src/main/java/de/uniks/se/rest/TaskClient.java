package de.uniks.se.rest;

import de.uniks.se.model.Task;

import java.util.function.Function;

import static de.uniks.se.rest.RestUtils.*;

public class TaskClient {


    public static void putTask(Task updateTask, Function<Task, Void> callback) {
        String url = DB_BASE_URL + DB_TASK + "/" + updateTask.get_id();

        try {
            String jsonString = HttpClient.deserialize().writeValueAsString(updateTask);
            String updatedTaskJson = HttpClient.put(url, jsonString);

            Task updatedTask = HttpClient.deserialize().readValue(updatedTaskJson, Task.class);
            callback.apply(updatedTask);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
