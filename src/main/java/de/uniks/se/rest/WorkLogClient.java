package de.uniks.se.rest;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import de.uniks.se.model.DBObject;
import de.uniks.se.model.WorkLog;

import java.util.function.Function;

import static de.uniks.se.rest.RestUtils.*;

public class WorkLogClient {

    private static JsonParser parser = new JsonParser();

    public static void postWorkLog(WorkLog log, Function<WorkLog, Void> callback) {
        String url = DB_BASE_URL + DB_WORK_LOG;

        try {
            String jsonString = HttpClient.deserialize().writeValueAsString(log);

            // remove unwanted values when posting
            JsonObject json = parser.parse(jsonString).getAsJsonObject();
            json.remove(DBObject.PROPERTY___v);
            json.remove(DBObject.PROPERTY__id);
            json.remove(DBObject.PROPERTY_created_date);
            json.remove(DBObject.PROPERTY_last_modified_date);
            jsonString = json.toString();

            String newLogJson = HttpClient.post(url, jsonString);

            WorkLog newLog = HttpClient.deserialize().readValue(newLogJson, WorkLog.class);
            System.out.println(newLog);
            callback.apply(newLog);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
