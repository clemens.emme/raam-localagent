package de.uniks.se.rest;

import com.google.gson.JsonObject;
import de.uniks.se.model.Credential;
import de.uniks.se.model.Person;
import de.uniks.se.model.Repository;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;

import java.io.IOException;
import java.util.ArrayList;
import java.util.function.Function;

import static de.uniks.se.rest.RestUtils.*;

public class UserClient {

    private static ArrayList<Person> persons;
    private static Person person;
    private static Person user;

    // ================================================================================================================
    // Login User
    // ================================================================================================================

    public void logout() {
        user = null;
    }

    public void login(String username, String password, Function<Person, Void> callback) {
        String url = DB_BASE_URL + DB_PERSON + DB_USER_LOGIN;

        JsonObject json = new JsonObject();
        json.addProperty("username", username);
        json.addProperty("password", password);

        String res = HttpClient.post(url, json.toString());

        try {
            user = HttpClient.deserialize().readValue(res, Person.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        callback.apply(user);
    }

    public static UsernamePasswordCredentialsProvider getUserCredentials(Repository repository) {
        Credential correctCredentials = null;

        for (Credential credential : user.getCredentials()) {
            // Check for same Id
            if (repository.get_id().equals(credential.getRepository().get_id())) {
                correctCredentials = credential;
            }
        }

        if (correctCredentials == null) {
            return null;
        }

        return new UsernamePasswordCredentialsProvider(correctCredentials.getUsername(), correctCredentials.getPassword());
    }

    // ================================================================================================================
    // Rest
    // ================================================================================================================

    public void getAll(Function<ArrayList<Person>, Void> callback) {
        String url = DB_BASE_URL + DB_PERSON;

        String res = HttpClient.get(url);


        persons = (ArrayList<Person>) HttpClient.deserializeList(res, Person.class);

        callback.apply(persons);
    }


    public void getOneById(String id, Function<Person, Void> callback) {
        String url = DB_BASE_URL + DB_PERSON + "/" + id;

        String res = HttpClient.get(url);

        person = HttpClient.serialize().fromJson(res, Person.class);

        callback.apply(person);
    }
}
