package de.uniks.se.rest;

public class RestUtils {

    // Base
    public static final String DB_BASE_URL = "http://avocado.uniks.de:9494/api";
//    public static final String DB_BASE_URL = "http://localhost:9090/api";
    // Add Ons
    public static final String DB_ROLE = "/role";
    public static final String DB_TASK = "/task";
    public static final String DB_PERSON = "/person";
    public static final String DB_WORK_LOG = "/workLog";
    public static final String DB_TASKTYPE = "/taskType";
    public static final String DB_CONTAINER = "/container";
    public static final String DB_REPOSITORY = "/repository";


    public static final String DB_USER_LOGIN = "/login";
}
